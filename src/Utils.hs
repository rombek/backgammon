module Utils where

import System.Random
import Consts
import Types

--

-- Function that immitates dices throwing
getDiceStream :: StdGen -> Stream (Int, StdGen) 
getDiceStream gen =   
  StreamC (newn, newgen) (getDiceStream newgen)
  -- Get new random number and generator.
  where (newn, newgen) = randomR diceRange gen

-- Take some values from infinite stream
throwDice :: Stream (Int, StdGen) -> (Int, Stream (Int, StdGen))
throwDice (StreamC x xs) = (fst x, xs)