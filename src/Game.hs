module Game where

import Types

-- Check if move by current player possible 
isMovePossible:: AppState -> Bool
isMovePossible _ = True

-- Returns all possible move for piece and dices roll
showPossibleMoves:: Board -> Piece -> MoveInfo -> [Piece]
showPossibleMoves _ _ _ = []

-- Is any move possible for current player
-- isAnyMovePossible :: AppState -> MoveInfo

-- Make one move
makeMove:: MoveInfo -> AppState -> AppState
makeMove _  x = x
