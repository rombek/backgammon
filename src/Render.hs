module Render where

import Graphics.Gloss.Interface.Pure.Game
import Types


-- Find if some piece was clicked
-- findClickedPiece:: (Int, Int) -> [Piece] -> Maybe Piece


handleEvent :: Event -> MoveInfo
handleEvent _ = MoveInfo 1 (-1, -1)

-- drawApp :: AppState -> Picture

-- Simulation step (updates nothing).
updateApp :: Float -> AppState -> AppState
updateApp _ x = x