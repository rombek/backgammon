module Types where

-- import Graphics.Gloss.Data.Point
-- import Graphics.Gloss.Data.Display
import Graphics.Gloss.Data.Color

import System.Random

-- Color of players pieces and board
data ColorConfig = ColorConfig
  { colorP1 :: Color
  , colorP2 :: Color
  , colorBackground:: Color
  }

-- One piece from game board
data Piece = Piece
    {
        piecePlayerId :: Int, -- Who owns the piece
        pieceX :: Int, -- Coordinates of piece on screen.
        pieceY :: Int
    }

-- One cell from game board
data BoardCell = BoardСell
    {
        pieces :: [Maybe Piece], -- List of pieces in cell
        cellX :: Int, -- Coordinates of cell on screen.
        cellY :: Int
    }

-- Game board
data Board = Board 
    {
        cells :: [BoardCell]
    }

-- General application state.
data AppState = AppState
    { 
        curPlayer :: Int, -- Shows which turn now
        turnNumber :: Int, -- Total number of steps taken
        randomGen :: StdGen, -- Random number generator
        colors :: ColorConfig, -- Colors config
        board :: Board, -- Board state
        allPieces :: [Piece] -- list of all pieces in game
    }

-- Polymorphic Stream
data Stream a = StreamC a (Stream a) deriving Show


-- Information about move
data MoveInfo = MoveInfo
    {
        movePlayerId :: Int,
        dicesRoll :: (Int, Int)
    }