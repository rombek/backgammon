module Consts where

import Graphics.Gloss.Interface.Pure.Game

-- Minimum and maximum dice value
diceRange:: (Int, Int)
diceRange = (1, 6)

pieceRadius:: Int
pieceRadius = 5

-- Path to config file.
configPath :: FilePath
configPath = "config.txt"

-- Game display mode.
display :: Display
display = FullScreen

-- Background color.
bgColor :: Color
bgColor = black

-- Simulation steps per second.
fps :: Int
fps = 60

-- Text shift on screen.
textShift :: Float
textShift = 250

