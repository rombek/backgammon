module Main where

import Utils
import System.Random

main :: IO ()
main = do
    gen <- newStdGen
    let diceStream = getDiceStream gen
    let (res1, diceStream1) = throwDice diceStream
    print $ res1

    let (res2, _) = throwDice diceStream1
    print $ res2